#!/usr/bin/env bash

echo "Installing dependencies and some python packages"

sudo apt-get update
sudo apt-get install build-essential python-pip python-dev python-smbus git
sudo apt-get install -y libpython-dev

echo "Installing Adafruit_Python_GPIO library"

cd ~
git clone https://github.com/adafruit/Adafruit_Python_GPIO.git
cd Adafruit_Python_GPIO
sudo python setup.py install
cd ~
sudo rm -r Adafruit_Python_GPIO

echo "Installing BME280 library"

git clone https://github.com/adafruit/Adafruit_Python_BME280
cd Adafruit_Python_BME280
sudo python setup.py install
cd ~
sudo rm -r Adafruit_Python_BME280