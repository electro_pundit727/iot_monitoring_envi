from Adafruit_BME280 import *

sensor = BME280(t_mode=BME280_OSAMPLE_8, p_mode=BME280_OSAMPLE_8, h_mode=BME280_OSAMPLE_8, address=0x76)


def read_bme280():
    """
    Read sensor data from BME280
    :return: (temperature, humidity, pressure)
    """
    try:
        # Perform soft reset
        sensor._device.write8(0xE0, 0xB6)
        time.sleep(.1)

        degrees = sensor.read_temperature()
        pascals = sensor.read_pressure()
        hectopascals = pascals / 100
        humidity = sensor.read_humidity()
        return degrees, humidity, hectopascals
    except:
        return None, None, None

# print 'Temp      = {0:0.3f} deg C'.format(degrees)
# print 'Pressure  = {0:0.2f} hPa'.format(hectopascals)
# print 'Humidity  = {0:0.2f} %'.format(humidity)


if __name__ == '__main__':

    import pprint
    while True:
        pprint.pprint(read_bme280())
        time.sleep(1)